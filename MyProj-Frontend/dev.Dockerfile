FROM node:15.11.0-alpine3.10
WORKDIR /app
COPY ./package.json package.json
COPY ./yarn.lock yarn.lock
RUN yarn
COPY ./src src
COPY ./public public
CMD yarn start
