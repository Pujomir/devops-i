FROM node:15.11.0-alpine3.10 as build_step
COPY . /app
WORKDIR /app
RUN yarn
RUN yarn run build

FROM nginx:1.19
COPY --from=build_step /app/build /usr/share/nginx/html
