FROM node:15.11.0-alpine3.10
WORKDIR /app
COPY ./package.json package.json
COPY ./yarn.lock yarn.lock
COPY ./tsconfig.json tsconfig.json
COPY ./tslint.json tslint.json
RUN yarn
COPY ./src src
CMD yarn start
